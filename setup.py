#!/usr/bin/env python

import os
from distutils.core import setup, Extension

pwd = os.path.dirname(os.path.realpath(__file__))

setup(
    name='cFiber',
    version='0.1.0',
    description='Fiber implementation for python in C++',
    author='Sameer Rahmani',
    author_email='lxsameer@gnu.org',
    url='https://gitlab.com/lxsameer/cfiber',
    package_dir={'cFiber': 'lib/python/cFiber'},
    packages=['cFiber'],
    headers=['includes/cfiber.hpp', 'includes/barrier.hpp'],
    license='MIT',
    install_requires=[
        'cppyy',
    ],
    ext_modules=[
        Extension(
            'cfiber',
            ['lib/cpp/cfiber.cpp'],
            include_dirs=['%s/includes' % pwd],
            language='C++',
            libraries=['boost_fiber', 'boost_context', 'pthread'],
            extra_compile_args=[
                '-Wall',
                '-Wextra',
                '-Wno-unused-parameter',
                '-Wno-missing-field-initializers',
                '-Wno-write-strings',
                '-std=gnu++14',
                '-O0',
                '-g',
                '-ggdb'
            ],
        )
    ]
)
