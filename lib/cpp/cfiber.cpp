#include <iostream>
#include <boost/fiber/all.hpp>
#include <unistd.h>
#include <typeinfo>
#include <Python.h>

#include "cfiber.hpp"
#include "barrier.hpp"

using namespace std;

static atomic<int> fiber_count{0};
static mutex mtx_count{};
static boost::fibers::condition_variable_any stop_condition{};
static vector<task> fn_queue;
static vector<thread> worker_threads;
static thread main_thread;
static atomic<bool> stopped(false);
static task_channel ch{2048};
static callback_channel callback_ch{2048};
static vector<exception> exception_vector{};
static atomic<bool> engine_status{false};

void fiber_runner(task f) {
  try {
    f();
  } catch(exception& e) {
    exception_vector.push_back(e);
  }
}

void fiber_python_callable_runner(PyObject *f) {
  if (!PyCallable_Check(f)) {
    PyErr_SetString(PyExc_TypeError, "Need a callable object.");
    return;
  }

  PyGILState_STATE gstate;
  gstate = PyGILState_Ensure();

  PyObject *result = PyObject_CallObject(f, NULL);
  PyObject *ex = PyErr_Occurred();

  if(ex) {
    PyErr_Print();
  } else {
    PyErr_Clear();
  }

  PyGILState_Release(gstate);
  cout << ".";
}

std::vector<std::exception> *get_exceptions() {
  return &exception_vector;
}


bool register_fiber(task f) {
  if (ch.push(f) == boost::fibers::channel_op_status::success) {
    return true;
  }

  return false;
}

bool register_python_callable(PyObject *callback) {
  if(callback_ch.push(callback) == boost::fibers::channel_op_status::success) {
    return true;
  }

  return false;
}


void worker_thread_fn(uint32_t number_of_threads, barrier *b) {
  PyObject *f;
  cout << "WORKER: " << std::this_thread::get_id() << endl;
  boost::fibers::use_scheduling_algorithm<boost::fibers::algo::work_stealing>(number_of_threads);
  b->wait();

  // for(task f : ch) {
  //   boost::fibers::fiber([f](){fiber_runner(f);}).detach();
  // }

  while (boost::fibers::channel_op_status::success == callback_ch.pop(f)) {
    boost::fibers::fiber([f](){fiber_python_callable_runner(f);}).detach();
  }

  cout << "TERMINATING WORKER: " << std::this_thread::get_id() << endl;
}


void main_thread_fn(uint32_t number_of_threads) {
  PyObject *f;
  cout << "MAIN: " << std::this_thread::get_id() << endl;
  boost::fibers::use_scheduling_algorithm< boost::fibers::algo::work_stealing >(number_of_threads);
  cout << "Spawning the threadpool" << endl;

  barrier b(number_of_threads);

  for (uint32_t i = 1; i < number_of_threads ; ++i) {
    worker_threads.push_back(thread(worker_thread_fn, number_of_threads, &b));
  }

  b.wait();

  // for(task f : ch) {
  //   cout << "Read from the channel main" << endl;
  //   boost::fibers::fiber([f](){fiber_runner(f);}).detach();
  // }
  while (boost::fibers::channel_op_status::success == callback_ch.pop(f)) {
    boost::fibers::fiber([f](){fiber_python_callable_runner(f);}).detach();
  }

  cout << "Main exit" << endl;
  BOOST_ASSERT( 0 == fiber_count);

}


int start_engine() {
  stopped = false;
  engine_status = true;
  uint32_t number_of_cpus = thread::hardware_concurrency();

  cout << "main thread started " << this_thread::get_id() << endl;
  cout << "CPUs: " << number_of_cpus << endl;

  main_thread = thread(main_thread_fn, number_of_cpus);

  return 0;
}

int stop_engine() {
  stopped = true;
  engine_status = false;
  ch.close();
  callback_ch.close();

  main_thread.join();
  for(thread &i : worker_threads) {
    i.join();
  }

  BOOST_ASSERT( 0 == fiber_count);
  cout << "done." << endl;
  return 0;
}


static PyObject *cfiber_start(PyObject *self, PyObject *args) {
  if (engine_status) {
    PyErr_SetString(PyExc_TypeError, "Engine is already running.");
    return 0;
  }

  if(start_engine() == 0) {
    return Py_True;
  }

  return Py_False;
}

static PyObject *cfiber_stop(PyObject *self, PyObject *args) {
  if (!engine_status) {
    PyErr_SetString(PyExc_TypeError, "Engine is already stopped.");
    return 0;
  }

  if(stop_engine() == 0) {
    return Py_True;
  }

  return Py_False;
}


static PyObject *cfiber_register(PyObject *self, PyObject *args) {
  PyObject *callback;

  if (!PyArg_ParseTuple(args, "O", &callback))
    return NULL;

  if (!PyCallable_Check(callback)) {
    PyErr_SetString(PyExc_TypeError, "Need a callable object.");
    return 0;
  }

  if(register_python_callable(callback)) {
    return Py_True;
  }

  return Py_False;
}


// module's function table
static PyMethodDef cfiber_functions_table[] = {
    {
      "start",
      cfiber_start,
      METH_VARARGS,
      "Starts the fiber engine and returns a boolean indicating the status of the engine."
    },
    {
      "stop",
      cfiber_stop,
      METH_VARARGS,
      "Stops the fiber engine and returns a boolean indicating the status of the engine."
    },
    {
      "register",
      cfiber_register,
      METH_VARARGS,
      "Register a callable object to be executed in the fiber pool."
    },

    {
        NULL, NULL, 0, NULL
    }
};

// modules definition
static struct PyModuleDef cfiber = {
    PyModuleDef_HEAD_INIT,
    "cfiber",
    "A C++ extension to run python functions in fibers.",
    -1,
    cfiber_functions_table
};

PyMODINIT_FUNC PyInit_cfiber(void) {
    return PyModule_Create(&cfiber);
}
