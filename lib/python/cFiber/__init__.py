from .base import FiberRunner

fibers = FiberRunner()

__all__ = [fibers]
