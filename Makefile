run:
	g++ -o cfiber lib/cpp/cfiber.cpp  -lboost_fiber -lboost_context -lpthread -std=c++17 -I./includes
	./cfiber
clean:
	rm -rf cfiber *~ *.o *.so
so:
	g++ -o cfiber.so -fPIC -shared ./lib/cpp/cfiber.cpp -lboost_fiber -lboost_context -lpthread -std=c++17 -I./includes -I/home/lxsameer/.pyenv/versions/3.5.6/include/python3.5m -g  -g3 -ggdb
pyreq:
	virtualenv .env

install:
	mv -v cfiber.so $(LIB_PATH)
	mv -v includes/ $(HEADER_PATH)/cfiber/
