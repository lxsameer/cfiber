#ifndef FIBERTEST_H
#define FIBERTEST_H

#include <boost/fiber/all.hpp>
#include <Python.h>

#include "barrier.hpp"

typedef std::unique_lock<std::mutex> lock_type;
typedef void(*task)();
typedef boost::fibers::buffered_channel<task> task_channel;
typedef boost::fibers::buffered_channel<PyObject*> callback_channel;

void print_queue();
bool register_fiber(task f);
bool register_python_callable(PyObject*);
void worker_thread_fn(std::uint32_t number_of_threads, barrier *b);
void main_thread_fn(std::uint32_t number_of_threads);
void fiber_runner(task f);
void fiber_python_callable_runner(PyObject*);
std::vector<std::exception> *get_exceptions();
int start_engine();
int stop_engine();

#endif // FIBERTEST_H
