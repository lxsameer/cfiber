import cppyy
import cppyy.ll



class FiberRunner:
    def __init__(self):
        cppyy.include('cFiber/cfiber.hpp')
        cppyy.load_library('cfiber')
        cppyy.ll.set_signals_as_exception(True)

    def register(self, runnable):
        is_registered = False
        with cppyy.ll.signals_as_exception():
            is_registered = cppyy.gbl.register_fiber(runnable)

        return is_registered

    def start(self):
        cppyy.gbl.start_engine()

    def stop(self):
        cppyy.gbl.stop_engine()

    def exceptions(self):
        return cppyy.gbl.get_exceptions();
