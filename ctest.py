import os
import cppyy
import threading

cppyy.add_include_path(os.path.dirname(os.path.realpath(__file__)))

class TestClass():
    def __call__(self):
        print("Python in thread: {}".format(threading.current_thread().ident))


test_instance = TestClass()

def run():
    cppyy.include('cfiber.hpp')
    cppyy.load_library('cfiber')
    for i in range(100):
        cppyy.gbl.register_task(test_instance)

    cppyy.gbl.print_queue()

if __name__ == "__main__":
    run()
